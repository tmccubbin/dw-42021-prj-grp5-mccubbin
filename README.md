## 420-420-DW Sec 01 Python - Server Side Programming
#### Project 2 - Django Products Website

*Created by: Trevor McCubbin ID 1542960 & Kristina Amend ID 0735252 & Emilia Krichevsky ID 19418981 & Azadeh Ahmadi ID 1811386*

*Version: 1.0*

**An application written in Python that involves the django_framework and a postgres database**

### :computer: :convenience_store: :email:

* A store-front for some beautiful hand-made soaps
* Users List will display all the users that the logged in user has the ability to modify. Non-admins do not have access to this page
* Messaging allows you to message other users on the platform
* Profile allows the user to modify their profile picture and change some key information about their account


## Live sample of Project:

A live sample of the project can be viewed at:

**mtlsoapco.herokuapp.com**

## How to run:

0. Download this project (clone the repo) or from the Moodle/LEA submission

**https://gitlab.com/tmccubbin/dw-42021-prj-grp5-mccubbin.git**

#### The user will need to setup their python virtual environment
1. Create a virtual environment
2. Install all applications within requirements.txt

#### The user will need to enter their Postgres SQL credentials in one file:
1. Database: Line 87 [ Products/Products/settings.py ]
2. User: Line 88 [ Products/Products/settings.py ]
3. Password: Line 89 [ Products/Products/settings.py ]

#### To run the application
1. Create a database in postgres with the same name that you entered on Line 87 of [ Products/Products/settings.py ]
2. Open CMD (Terminal) in the main project directory [ Products ]
3. Run the following command: py makemigrations Administration
4. Run the following command: py migrate Administration
5. Run the following command: py makemigrations
6. Run the following command: py migrate
7. Run the following command: py manage.py runserver
8. Proceed to 127.0.0.1:8000 on your favorite browser

**_Any exceptions will be displayed in the console_**
* _You may encounter errors when the database does not exit or cannot be found_
* _You may also encounter errors if you do not migrate the models properly_
