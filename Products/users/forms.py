from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from .models import Profile

User = get_user_model()


# === AUTHOR: KRISTINA AMEND === #

# Class based view to create a user registration form
class UserRegisterForm(UserCreationForm):  # the input is the inherited class - UserReg inherits from UserCreForm
    email = forms.EmailField()  # the default is true

    class Meta:  # here we are saying the model that will be affected will by the User model
        model = User
        # the fields we want shown, and in what order
        fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password2']


# Class based view to update the users username + email
class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']


# Class based view to update the users profile picture
class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']
