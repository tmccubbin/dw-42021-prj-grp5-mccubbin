from django.contrib import admin
from .models import Profile


# registers the Profile view in .users so it is visible on the admin page
admin.site.register(Profile)
