from django.db import models
from django.conf import settings
from PIL import Image
User = settings.AUTH_USER_MODEL


# === AUTHOR: KRISTINA AMEND === #

# Creates a 1-1 relationship w/ existing User model
# Adds an image field so the user can upload a picture
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.png', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username} Profile'

    # Function that overrides the save method to resize images on save
    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        # opens the current image that is being saved
        img = Image.open(self.image.path)
        if img.height > 300 or img.width > 300:
            # tuple of max size dimensions
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)
