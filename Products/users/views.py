from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from .models import Profile

from django.contrib.auth import get_user_model

# Get the User model currently being used by the app (needed because an abstract user model is defined)
User = get_user_model()

# === AUTHOR: KRISTINA AMEND === #

# Function based view that renders the request w/ user input fields
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            profile = Profile.objects.get_or_create(user=User.objects.get(username=username))
            # flash message below for one time alert (needs to be imported
            # messages.debug, messages.info, messages.success, messages.warning, messages.error
            messages.success(request, f'Your account has been created! You can now login.')  # f string
            # return redirect('home') we want to return to home page once successfully logged in
            return redirect('login')  # return to login page when account created

    else:
        form = UserRegisterForm()  # create an instance of the form class
        # render template that uses this form
        return render(request, 'users/register.html', {'form': form})


# Function based view that renders the request to update a users profile
# User must be logged in to view this page
@login_required
# adds functionality to existing function. user must be logged in to view this page
def profile(request):
    if request.method == 'POST':
        # create an instance of the currently logged in user (fills the fields)
        user_form = UserUpdateForm(request.POST, instance=request.user)
        profile_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        # only save form data if valid
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, f'Your account has been updated.')
            return redirect('profile')
    else:
        user_form = UserUpdateForm(instance=request.user)  # create an instance of the currently logged in user
        profile = Profile.objects.get_or_create(user=request.user)
        profile_form = ProfileUpdateForm(instance=request.user.profile)
    context = {
        'user_form': user_form,
        'profile_form': profile_form,
    }
    return render(request, 'users/profile.html', context)
