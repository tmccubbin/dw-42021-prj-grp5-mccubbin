# Azadeh Ahmadi (1811386)
from django.urls import path

from .views import *

urlpatterns = [path("inbox/", MessageList.as_view(), name='inbox'),
path("message/sent/", SentMessageList.as_view(), name='sent'),
path("message/compose/", ComposeMessage.as_view(), name='compose'),
path("message/searchEmail/", SearchEmail.as_view(), name='email_search'),
path("message/<int:pk>/reply/", ReplyMessage.as_view(), name='reply'),
path("message/<int:pk>/forward/", ForwardMessage.as_view(), name='forward'),
path("message/<int:pk>/", MessageDetail.as_view(), name='detail'),
path("message/<int:pk>/delete", DeleteMessage.as_view(), name='delete'),
path("notification/<int:pk>/", DeleteNotificationView, name='delete-notif'),
path("create-notif/<int:pk>/",CreateNotification.as_view(),name='create-notif')


               ]