# Azadeh Ahmadi (1811386)
from nturl2path import pathname2url
from pathlib import Path
from urllib import request
from datetime import datetime
import logging
# Get an instance of a logger


from django.conf.urls import url
from django.contrib.messages.views import SuccessMessageMixin

logger = logging.getLogger('admin')
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
#from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()
from django.db.models import Q
import logging
from django.shortcuts import render, redirect

from django.shortcuts import render, get_object_or_404
from django.views import View
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView, TemplateView

#from .models import Comments
#from .forms import Comments_Form

# def itemDetail(request):
#     template_name = 'item_Detail.html'
#     item = get_object_or_404(Item)
#     comments = item.comments
#
#     # Comment posted
#     if request.method == 'POST':
#         if request.user.is_authenticated:
#             form = Comments_Form(data=request.POST)
#             comments=item.comments
#             if form.is_valid():
#
#                 # Create Comment object but don't save to database yet
#                 new_comment = form.save(commit=False)
#                 # Assign the current post to the comment
#                 new_comment.item = item
#                 # Save the comment to the database
#                 new_comment.save()
#             else:
#                 form = Comments_Form()
#
#         return render(request, template_name, {'item': item,
#                                                'comments': comments,
#                                                'new_comment': new_comment,
#                                                'comment_form': Comments_Form})
#
#     class item_detail(DetailView):
#         model=Item
#
#         def get_context_data(self, **kwargs):
#             context=super.get_context_data(**kwargs)
#             comments=Comments.object.filter(item=self.get_object()).order_by('date')
#
#             if self.request.user.is_authenticated:
#                 new_form = Comments_Form(instance=self.request.user)
#             context = context + {'comments': comments,
#                                  'form':new_form
#                                  }
#             return context
#         def post(self, request, *args, **kwargs):
#             new_comment = Comments(body=request.POST.get('text'),
#                                    item=self.get_object(),
#                                       member=self.request.user
#                                       )
#             new_comment.save()
#             return self.get(self, request, *args, **kwargs)
#
#
#
#from .models import Message
from .forms import Message_Form
from .models import Message,Notification


class MessageList(LoginRequiredMixin,ListView):
    model = Message
    paginate_by = 6
    ordering=['date']



    def get_queryset(self,*args,**kwargs):


        return Message.objects.filter(receiver=self.request.user)

class MessageDetail(LoginRequiredMixin,DetailView):
        model = Message





class SentMessageList(LoginRequiredMixin,ListView):
        model = Message
        paginate_by = 10
        ordering = ['date']
        paginate_by = 6


        def get_queryset(self, *args, **kwargs):
            return Message.objects.filter(sender=self.request.user)
        def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            context['name']='sent'
            return context




@login_required
def DeleteNotificationView(request,pk):
    post2delete = Notification.objects.get(id=pk)
    post2delete.delete()

    return redirect('home')


class DeleteMessage(UserPassesTestMixin,LoginRequiredMixin,SuccessMessageMixin,DeleteView):
    model=Message
    success_url='../../inbox'
    success_message='The message was deleted'

    def test_func(self):
        message=self.get_object()
        return (self.request.user==message.receiver or self.request.user==message.sender)

    def get_success_url(self, **kwargs):
       if self.get_object().receiver==self.request.user:
            return ('../../inbox')
       else:
            return ('../../message/sent')




class ComposeMessage( SuccessMessageMixin,LoginRequiredMixin,CreateView):
    model=Message
    success_url = '../../inbox/'
    fields=['receiver','title','text']
    success_message = 'The  message was sent! '
    def form_valid(self,form):
        form.instance.sender=self.request.user
        return super().form_valid(form)


    def get_context_data(self, **kwargs):
             context = super().get_context_data(**kwargs)

             context['create_update_legend']='Compose '
             context['form_submit_btn'] = 'Send'
             return context




# class ReplyMessage(LoginRequiredMixin, CreateView):
class ReplyMessage(SuccessMessageMixin,LoginRequiredMixin,CreateView):
        model = Message
        success_url = '../../sent/'
        fields = ['title','text']
        success_message="The Reply was sent!"
        def form_valid(self, form):
            form.instance.sender = self.request.user
            form.instance.receiver=self.get_object().sender
            return super().form_valid(form)

        def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)

            context['create_update_legend'] = 'reply'
            context['form_submit_btn'] = 'Send'
            return context


# class ForwardMessage(LoginRequiredMixin,UserPassesTestMixin, CreateView):
class ForwardMessage(LoginRequiredMixin,TemplateView):
    model = Message
    success_url = '../../sent/'
    fields = ['receiver','text']
    template_name = 'messaging/message_form.html'
    def post(self, request, pk):
        message = Message.objects.get(id=pk)
        sender=message.receiver
        text=message.text
        new_message=Message(text=text,sender=sender,title=message.title)
        form = Message_Form(request.POST, instance=new_message)
        if form.is_valid():
            form.save()
            messages.success(request, ' Message was Forwarded to  '+str(form.instance.receiver))
            return redirect('sent')


    def get_context_data(self,pk ,**kwargs):

        msg = Message.objects.get(id=pk)
        new_message=Message(text=msg.text,sender=msg.receiver,title=msg.title)
        form = Message_Form( instance=new_message)
        text=msg.text
        title=msg.title
        context = super().get_context_data(**kwargs)
        context['title']=title
        context['form']=form
        context['text'] = text
        context['create_update_legend'] = 'Forward'
        context['form_submit_btn'] = 'Forward'



        return context


class SearchEmail(ListView):
   model=Message
   template_name='message_list.html'
   paginate_by = 6

   def get_queryset(self):
       query = self.request.GET.get("query")
       query2 = self.request.GET.get("query2")
       count=0
       if query:
           lst=Message.objects.filter(Q(receiver__username__icontains=query),Q(sender=self.request.user))
           count = len(lst)
           messages.success(self.request, "we found {0} results for query {1}".format(count, query))
           return lst
       elif query2:
           lst = Message.objects.filter( Q(sender__username__icontains=query2),Q(receiver=self.request.user))
           count = len(lst)
           messages.success(self.request, "we found {0} results for query {1}".format(count, query2))
           return lst
       else:
           return Message.objects.all()

   def get_context_data(self,  **kwargs):
       query = self.request.GET.get("query")
       query2 = self.request.GET.get("query2")
       context = super().get_context_data(**kwargs)
       if query:
           context['name']='search'
       return context
from .forms import Notification_Form



class CreateNotification(TemplateView,LoginRequiredMixin):
    template_name ="messaging/notification_form.html"




    def get_context_data(self, pk, **kwargs):
        user = User.objects.get(id=pk)
        notification=Notification(user=user)
        form = Notification_Form(instance=notification)
        context = super().get_context_data(**kwargs)
        context['user']=user
        context['form'] = form
        return context
    def post(self, request, pk):
        user = User.objects.get(id=pk)
        notification=Notification(user=user)
        form = Notification_Form(request.POST, instance=notification)
        if form.is_valid():
            form.save()
            messages.success(request, ' nitification was sent to : ' + str(user.username))
            logger.info(str(datetime.now()) + ' nitification was sent to : <' + str(user.username) + '>')
            return redirect('administration')




