
#from .models import Comments
from django import forms

# Azadeh Ahmadi (1811386)
# class Comments_Form(forms.ModelForm):
#     class Meta:
#         model = Comments
#         fields = ('text')
from .models import Message,Notification


class Message_Form(forms.ModelForm):
            class Meta:
                model = Message
                fields = ('receiver','title','text')


class Notification_Form(forms.ModelForm):
    class Meta:
        model = Notification
        fields = ('user','message',)
