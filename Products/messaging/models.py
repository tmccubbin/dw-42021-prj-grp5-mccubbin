# Azadeh Ahmadi (1811386)
from django.db import models
from django.contrib import messages

# Create your models here.
# class Comments(models.Model):
#     #item = models.ForeignKey(Item,on_delete=models.CASCADE,related_name='comments')
#     #member = models.ForeignKey(User,on_delete=models.CASCADE,related_name='comments')
#     text = models.TextField()
#     date = models.DateTimeField(auto_now_add=True)
#     active = models.BooleanField(default=False)
#
#     class Meta:
#         ordering = ['date']
#
#     def __str__(self):
#         return 'Comment {} by {} '.format(self.text, self.member,)
from django.urls import reverse
#from django.contrib.auth.models import User
from django.conf import settings
User = settings.AUTH_USER_MODEL



class Message(models.Model):
    sender = models.ForeignKey(User, related_name="sender" ,on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name="receiver",on_delete=models.CASCADE)
    title=models.CharField(default="",max_length=100)
    text = models.TextField(max_length=150)
    date = models.DateTimeField(auto_now_add=True)





class Meta:
    ordering = ['date']
def get_absolute_url(self):
    return reverse('message-detail', args=[str(self.id)])

class Notification(models.Model):
    user=models.ForeignKey(User, related_name="user",on_delete=models.CASCADE)
    message=models.TextField(max_length=150)

    def __str__(self):


        return self.message