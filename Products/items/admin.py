from django.contrib import admin
from .models import Item, Images, Category, Comment, Rate

# Register your models here.

admin.site.register(Item)
admin.site.register(Images)
admin.site.register(Category)
admin.site.register(Comment)
admin.site.register(Rate)




