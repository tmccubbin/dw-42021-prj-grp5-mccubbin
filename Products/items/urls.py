from django.urls import path
from . import views
from .views import (ItemListView, ItemCreateView,
                    ItemDetailView, ItemDeleteView,
                    ItemUpdateView, UserItemListView,
                    SearchResultsView)

urlpatterns = [

    path('', views.home, name='home'),
    path('items/', ItemListView.as_view(), name='items-all'),
    path('item/new/', ItemCreateView.as_view(), name='item-create'),
    path('item/<int:pk>/', ItemDetailView.as_view(), name='item-detail'),
    path('item/<int:pk>/delete/', ItemDeleteView.as_view(), name='item-delete'),
    path('item/<int:pk>/update/', ItemUpdateView.as_view(), name='item-update'),
    path('user/<str:username>/', UserItemListView.as_view(), name='user-items'),
    path('search/', SearchResultsView.as_view() , name='search'),
    path('category/<str:cat>/', views.category, name='category')
]


