from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from django.urls import reverse_lazy

from django.views.generic import (ListView, DetailView, CreateView,
                                  UpdateView, DeleteView)


from django.contrib.auth import get_user_model
from .models import Item, Comment, Rate, Category
from django.db.models import Q
from messaging.models import Notification
from .forms import AddCommentForm, EditForm, ItemForm, RateForm

User = get_user_model()


# === AUTHOR: EMILIA ===

def home(request):
    if request.user.is_authenticated:
        notifs = Notification.objects.filter(user=request.user)
        if (notifs):
            context = {'message': notifs[0]}
            return render(request, 'items/home.html', context)
        else:
            return render(request, 'items/home.html')

    else:
        return render(request, 'items/home.html')


def category(request, cat):
    category_items = Item.objects.filter(category__title=cat)
    return render(request, "items/category.html", {'cat': cat, 'category_items': category_items})


class SearchResultsView(ListView):
    model = Item
    template_name = 'items/search_results.html'

    def get_queryset(self):
        query = self.request.GET.get('search_bar')
        object_list = Item.objects.filter(
            Q(title__icontains='search_bar') | Q(category__icontains='search_bar') |
            Q(owner_username__icontains='search_bar')
        )


class ItemListView(ListView):
    model = Item
    template_name = 'items/index.html'
    context_object_name = 'items'
    fields = ['title', 'owner', 'category', 'image']
    paginate_by = 3
    ordering = ['title']



class ItemDetailView(LoginRequiredMixin, DetailView):

    model = Item

    # checks to see if the request user has this permission
    permission_required = 'items.view_item'

    # if the requesting user does not have the permission to view this page it redirects them to an error page
    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            return redirect('/administration/error')
        else:
            return redirect('/login')

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        reviews = Rate.objects.filter(item=self.get_object())
        comments = Comment.objects.filter(item=self.get_object()).order_by('-created_on')
        data['comments'] = comments
        data['reviews'] = reviews
        if self.request.user.is_authenticated:
            data['comment_form'] = AddCommentForm(instance=self.request.user)
            data['review_form'] = RateForm(instance=self.request.user)
        return data

    def form_valid(self, form):
        if 'new_comment' in self.request.POST:

            new_comment = Comment(subject=self.request.POST.get('subject'),
                                  comment=self.request.POST.get('comment'),
                                  user=self.request.user,
                                  item=self.get_object())
            new_comment.save()
            msg = "Your Comment was Added!"
        elif 'new_review' in self.request.POST:
            new_review = Rate(rate=self.request.POST.get('rate'),
                              user=self.request.user,
                              item=self.get_object()
                              )
            new_review.save()
            msg = "Your Review was Added!"

        messages.success(self.request, msg)
        return HttpResponseRedirect(self.request.path)


class ItemCreateView(PermissionRequiredMixin, CreateView):
    model = Item
    form_class = ItemForm
    template_name = 'items/item_create.html'

     # checks to see if the request user has this permission
    permission_required = 'items.add_item'

    # if the requesting user does not have the permission to view this page it redirects them to an error page
    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            return redirect('/administration/error')
        else:
            return redirect('/login')

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class ItemUpdateView(LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin, UpdateView):
    model = Item
    template_name = 'items/item_update.html'
    form_class = EditForm

     # checks to see if the request user has this permission
    permission_required = 'items.change_item'

    # if the requesting user does not have the permission to view this page it redirects them to an error page
    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            return redirect('/administration/error')
        else:
            return redirect('/login')
    
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

    def test_func(self):
        item = self.get_object()
        if self.request.user == item.owner:
            return True
        return False


class ItemDeleteView(LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin, DeleteView):
    model = Item
    success_url = reverse_lazy('items-all')

     # checks to see if the request user has this permission
    permission_required = 'items.delete_item'

    # if the requesting user does not have the permission to view this page it redirects them to an error page
    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            return redirect('/administration/error')
        else:
            return redirect('/login')

    def test_func(self):
        item = self.get_object()
        if self.request.user == item.owner:
            return True
        return False


class UserItemListView(ListView):
    model = Item
    template_name = 'items/user_items.html'
    context_object_name = 'items'
    paginate_by = 2

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Item.objects.filter(owner=user).order_by('-created_at')
