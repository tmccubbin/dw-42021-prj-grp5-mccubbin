#from django.contrib.auth.models import User
from django.conf import settings
User = settings.AUTH_USER_MODEL
from django.db import models
from django.urls import reverse
from django.db.models import Avg, Count


# Emilia K

class Category(models.Model):
    SOAP = 'Soap'
    CANDLE = 'Candle'
    LOTION = 'Lotion'
    # if entry_object.status == Entry.LIVE_STATUS:
    #     # do something with live entry
    CATEGORY = (
        (SOAP, 'Bar Soap'),
        (CANDLE, '3-Wick Candle'),
        (LOTION, 'Lotion'),
    )
    title = models.CharField(choices=CATEGORY, default=SOAP, max_length=30)
    keywords = models.CharField(max_length=255)
    description = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.title


class Item(models.Model):
    AVAILABLE = 'Available'
    OUT_OF_STOCK = 'Out of Stock'
    # if entry_object.status == Entry.LIVE_STATUS:
    #     # do something with live entry
    STATUS = (
        (AVAILABLE, 'Available to Purchase'),
        (OUT_OF_STOCK, 'No Stock Available'),
    )
    title = models.CharField(max_length=150)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.CharField(max_length=200)
    status = models.CharField(choices=STATUS, default=AVAILABLE, max_length=32)
    price = models.DecimalField(max_digits=12, default=0, decimal_places=2)
    address = models.CharField(max_length=150)
    image = models.ImageField(upload_to='items/', default='default/tropicalmelon1.jpg')
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('item-detail', kwargs={'pk': self.pk})

    @property
    def number_of_comments(self):
        return Comment.objects.filter(item=self).count()

    @property
    def get_available(self):
        return Item.objects.filter(status=Item.AVAILABLE_STATUS)

    @property
    def get_unavailable(self):
        return Item.objects.filter(status=Item.OUT_STATUS)

    @property
    def get_count_review(self):
        reviews = Rate.objects.filter(item=self).aggregate(count=Count('id'))
        count = 0
        if reviews["count"] is not None:
            count = int(reviews["count"])
        return count

    @property
    def get_avg_review(self):
        reviews = Rate.objects.filter(item=self).aggregate(average=Avg('rate'))
        avg = 0
        if reviews["average"] is not None:
            avg = float(reviews["average"])
        return avg


class Images(models.Model):
    title = models.CharField(max_length=75, blank=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    image = models.ImageField(blank=True, upload_to='default/')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Images'


"""
# then in your code
thing = get_my_thing()
thing.priority = ThingPriority.HIGH
"""


class Rate(models.Model):
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    RATE_CHOICES = (
        (ONE, 'One Star'),
        (TWO, 'Two Stars'),
        (THREE, 'Three Stars'),
        (FOUR, 'Four Stars'),
        (FIVE, 'Five Stars'),
    )
    rate = models.IntegerField(default=ONE, choices=RATE_CHOICES)
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name='reviews')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reviews')

    class Meta:
        verbose_name_plural = 'Rates'


class Comment(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name='comments')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')
    subject = models.CharField(max_length=50, blank=True)
    comment = models.CharField(max_length=250, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_on']
        verbose_name_plural = 'Comments'

    def __str__(self):
        return 'Comment: {} \n By {} '.format(self.subject, self.user)

    def get_absolute_url(self):
        return reverse('item-detail', kwargs={'pk': self.pk})
