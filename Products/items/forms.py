from .models import Comment, Item, Rate
from django import forms


class SearchItemForm(forms.Form):
    title = forms.CharField(max_length=40)

    class Meta:
        model = Item
        fields = ['title']


class AddCommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['subject', 'comment']


class RateForm(forms.ModelForm):
    class Meta:
        model = Rate
        fields = ['rate']


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['title', 'image', 'category', 'description', 'status', 'price', 'address']


class EditForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['title', 'description', 'status', 'price', 'address']
