--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Administration_userprofile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Administration_userprofile" (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    flagged boolean NOT NULL
);


ALTER TABLE public."Administration_userprofile" OWNER TO postgres;

--
-- Name: Administration_userprofile_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Administration_userprofile_groups" (
    id integer NOT NULL,
    userprofile_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public."Administration_userprofile_groups" OWNER TO postgres;

--
-- Name: Administration_userprofile_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Administration_userprofile_groups_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Administration_userprofile_groups_id_seq" OWNER TO postgres;

--
-- Name: Administration_userprofile_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Administration_userprofile_groups_id_seq" OWNED BY public."Administration_userprofile_groups".id;


--
-- Name: Administration_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Administration_userprofile_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Administration_userprofile_id_seq" OWNER TO postgres;

--
-- Name: Administration_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Administration_userprofile_id_seq" OWNED BY public."Administration_userprofile".id;


--
-- Name: Administration_userprofile_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Administration_userprofile_user_permissions" (
    id integer NOT NULL,
    userprofile_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public."Administration_userprofile_user_permissions" OWNER TO postgres;

--
-- Name: Administration_userprofile_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Administration_userprofile_user_permissions_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Administration_userprofile_user_permissions_id_seq" OWNER TO postgres;

--
-- Name: Administration_userprofile_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Administration_userprofile_user_permissions_id_seq" OWNED BY public."Administration_userprofile_user_permissions".id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: items_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_category (
    id integer NOT NULL,
    title character varying(30) NOT NULL,
    keywords character varying(255) NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE public.items_category OWNER TO postgres;

--
-- Name: items_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_category_id_seq OWNER TO postgres;

--
-- Name: items_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_category_id_seq OWNED BY public.items_category.id;


--
-- Name: items_comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_comment (
    id integer NOT NULL,
    subject character varying(50) NOT NULL,
    comment character varying(250) NOT NULL,
    created_on timestamp with time zone NOT NULL,
    item_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.items_comment OWNER TO postgres;

--
-- Name: items_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_comment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_comment_id_seq OWNER TO postgres;

--
-- Name: items_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_comment_id_seq OWNED BY public.items_comment.id;


--
-- Name: items_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_images (
    id integer NOT NULL,
    title character varying(75) NOT NULL,
    image character varying(100) NOT NULL,
    item_id integer NOT NULL
);


ALTER TABLE public.items_images OWNER TO postgres;

--
-- Name: items_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_images_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_images_id_seq OWNER TO postgres;

--
-- Name: items_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_images_id_seq OWNED BY public.items_images.id;


--
-- Name: items_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_item (
    id integer NOT NULL,
    title character varying(150) NOT NULL,
    description character varying(200) NOT NULL,
    status character varying(32) NOT NULL,
    price numeric(12,2) NOT NULL,
    address character varying(150) NOT NULL,
    image character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    category_id integer NOT NULL,
    owner_id integer NOT NULL
);


ALTER TABLE public.items_item OWNER TO postgres;

--
-- Name: items_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_item_id_seq OWNER TO postgres;

--
-- Name: items_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_item_id_seq OWNED BY public.items_item.id;


--
-- Name: items_rate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_rate (
    id integer NOT NULL,
    rate integer NOT NULL,
    item_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.items_rate OWNER TO postgres;

--
-- Name: items_rate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_rate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_rate_id_seq OWNER TO postgres;

--
-- Name: items_rate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_rate_id_seq OWNED BY public.items_rate.id;


--
-- Name: messaging_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messaging_message (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    text text NOT NULL,
    date timestamp with time zone NOT NULL,
    receiver_id integer NOT NULL,
    sender_id integer NOT NULL
);


ALTER TABLE public.messaging_message OWNER TO postgres;

--
-- Name: messaging_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.messaging_message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messaging_message_id_seq OWNER TO postgres;

--
-- Name: messaging_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.messaging_message_id_seq OWNED BY public.messaging_message.id;


--
-- Name: messaging_notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messaging_notification (
    id integer NOT NULL,
    message text NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.messaging_notification OWNER TO postgres;

--
-- Name: messaging_notification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.messaging_notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messaging_notification_id_seq OWNER TO postgres;

--
-- Name: messaging_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.messaging_notification_id_seq OWNED BY public.messaging_notification.id;


--
-- Name: users_profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_profile (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.users_profile OWNER TO postgres;

--
-- Name: users_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_profile_id_seq OWNER TO postgres;

--
-- Name: users_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_profile_id_seq OWNED BY public.users_profile.id;


--
-- Name: Administration_userprofile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile" ALTER COLUMN id SET DEFAULT nextval('public."Administration_userprofile_id_seq"'::regclass);


--
-- Name: Administration_userprofile_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_groups" ALTER COLUMN id SET DEFAULT nextval('public."Administration_userprofile_groups_id_seq"'::regclass);


--
-- Name: Administration_userprofile_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_user_permissions" ALTER COLUMN id SET DEFAULT nextval('public."Administration_userprofile_user_permissions_id_seq"'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: items_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_category ALTER COLUMN id SET DEFAULT nextval('public.items_category_id_seq'::regclass);


--
-- Name: items_comment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_comment ALTER COLUMN id SET DEFAULT nextval('public.items_comment_id_seq'::regclass);


--
-- Name: items_images id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_images ALTER COLUMN id SET DEFAULT nextval('public.items_images_id_seq'::regclass);


--
-- Name: items_item id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_item ALTER COLUMN id SET DEFAULT nextval('public.items_item_id_seq'::regclass);


--
-- Name: items_rate id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_rate ALTER COLUMN id SET DEFAULT nextval('public.items_rate_id_seq'::regclass);


--
-- Name: messaging_message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messaging_message ALTER COLUMN id SET DEFAULT nextval('public.messaging_message_id_seq'::regclass);


--
-- Name: messaging_notification id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messaging_notification ALTER COLUMN id SET DEFAULT nextval('public.messaging_notification_id_seq'::regclass);


--
-- Name: users_profile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_profile ALTER COLUMN id SET DEFAULT nextval('public.users_profile_id_seq'::regclass);


--
-- Data for Name: Administration_userprofile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Administration_userprofile" (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined, flagged) FROM stdin;
8	pbkdf2_sha256$216000$phzkD8nsYoRN$rQ9IKGbjhMN8T8J3AMnHpS33HKMHkCQ7rR1wVyT8wOE=	\N	f	azadeh	Azadeh	Ahmadi	azadeh@email.com	f	t	2021-05-24 19:38:19-04	t
6	pbkdf2_sha256$216000$phzkD8nsYoRN$rQ9IKGbjhMN8T8J3AMnHpS33HKMHkCQ7rR1wVyT8wOE=	\N	f	emilia	Emilia	Krichevsky	emilia@email.com	f	t	2021-05-24 19:37:10-04	t
5	pbkdf2_sha256$216000$phzkD8nsYoRN$rQ9IKGbjhMN8T8J3AMnHpS33HKMHkCQ7rR1wVyT8wOE=	2021-05-24 22:21:29.656913-04	f	trevor	Trevor	McCubbin	trevor@email.com	f	t	2021-05-24 19:35:42-04	t
7	pbkdf2_sha256$216000$phzkD8nsYoRN$rQ9IKGbjhMN8T8J3AMnHpS33HKMHkCQ7rR1wVyT8wOE=	\N	f	kristina	Kristina	Amend	kristina@email.com	f	t	2021-05-24 19:37:50-04	t
16	pbkdf2_sha256$216000$nr5N4SwDEQrT$8uoknbmbtaG0lxco6mc1SyH8EO34KaCIje5MQe3Zdok=	2021-05-24 21:05:38.838215-04	t	nasr	H	Nasreddine	nas@email.com	t	t	2021-05-24 21:01:48-04	f
17	pbkdf2_sha256$216000$phzkD8nsYoRN$rQ9IKGbjhMN8T8J3AMnHpS33HKMHkCQ7rR1wVyT8wOE=	2021-05-24 21:07:49.335502-04	f	trevor2	t	mccub	t@email.com	f	t	2021-05-24 21:06:17-04	f
1	pbkdf2_sha256$216000$gXCDUnqBl14H$wInGW2DnorPlGsdB/NzoHdGuhFuute5kd/Hr0bxj21c=	2021-05-24 21:07:59.586322-04	t	admin				t	t	2021-05-24 17:50:13.159243-04	f
10	pbkdf2_sha256$216000$phzkD8nsYoRN$rQ9IKGbjhMN8T8J3AMnHpS33HKMHkCQ7rR1wVyT8wOE=	\N	f	bob	Bob	Sinclair	bob@email.com	f	t	2021-05-24 19:44:03-04	f
9	pbkdf2_sha256$216000$phzkD8nsYoRN$rQ9IKGbjhMN8T8J3AMnHpS33HKMHkCQ7rR1wVyT8wOE=	\N	f	kevin	Kevin	Koueiki	kevin@email.com	f	t	2021-05-24 19:43:36-04	f
4	pbkdf2_sha256$216000$phzkD8nsYoRN$rQ9IKGbjhMN8T8J3AMnHpS33HKMHkCQ7rR1wVyT8wOE=	\N	f	tristen	Tristen	Lutchman	tristen@email.com	f	t	2021-05-24 19:34:29-04	f
3	pbkdf2_sha256$216000$phzkD8nsYoRN$rQ9IKGbjhMN8T8J3AMnHpS33HKMHkCQ7rR1wVyT8wOE=	\N	f	fred	Frederic	Lachacne	fred@email.com	f	t	2021-05-24 19:33:49-04	f
\.


--
-- Data for Name: Administration_userprofile_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Administration_userprofile_groups" (id, userprofile_id, group_id) FROM stdin;
2	3	1
3	4	1
4	5	4
5	6	3
6	7	3
7	8	2
8	9	2
9	10	1
10	16	4
11	17	1
12	5	1
13	6	1
14	7	1
15	8	1
16	9	1
17	16	1
18	1	4
19	1	1
\.


--
-- Data for Name: Administration_userprofile_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Administration_userprofile_user_permissions" (id, userprofile_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
1	members
2	admin_user_gp
3	admin_Item_gp
4	admin_gp
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	6
2	1	7
3	1	8
4	1	17
5	1	20
6	1	25
7	1	26
8	1	27
9	1	28
10	2	1
11	2	2
12	2	3
13	2	4
14	3	1
15	3	2
16	3	3
17	3	4
18	3	9
19	3	10
20	3	11
21	3	12
22	3	13
23	3	14
24	3	15
25	3	16
26	3	17
27	3	18
28	3	19
29	3	20
30	3	21
31	3	22
32	3	23
33	3	24
34	3	25
35	3	26
36	3	27
37	3	28
38	4	1
39	4	2
40	4	3
41	4	4
42	4	5
43	4	6
44	4	7
45	4	8
46	4	9
47	4	10
48	4	11
49	4	12
50	4	13
51	4	14
52	4	15
53	4	16
54	4	17
55	4	18
56	4	19
57	4	20
58	4	21
59	4	22
60	4	23
61	4	24
62	4	25
63	4	26
64	4	27
65	4	28
66	4	29
67	4	30
68	4	31
69	4	32
70	4	33
71	4	34
72	4	35
73	4	36
74	4	37
75	4	38
76	4	39
77	4	40
78	4	41
79	4	42
80	4	43
81	4	44
82	4	45
83	4	46
84	4	47
85	4	48
86	4	49
87	4	50
88	4	51
89	4	52
90	4	53
91	4	54
92	4	55
93	4	56
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add user	1	add_userprofile
2	Can change user	1	change_userprofile
3	Can delete user	1	delete_userprofile
4	Can view user	1	view_userprofile
5	Can add profile	2	add_profile
6	Can change profile	2	change_profile
7	Can delete profile	2	delete_profile
8	Can view profile	2	view_profile
9	Can add category	3	add_category
10	Can change category	3	change_category
11	Can delete category	3	delete_category
12	Can view category	3	view_category
13	Can add item	4	add_item
14	Can change item	4	change_item
15	Can delete item	4	delete_item
16	Can view item	4	view_item
17	Can add rate	5	add_rate
18	Can change rate	5	change_rate
19	Can delete rate	5	delete_rate
20	Can view rate	5	view_rate
21	Can add images	6	add_images
22	Can change images	6	change_images
23	Can delete images	6	delete_images
24	Can view images	6	view_images
25	Can add comment	7	add_comment
26	Can change comment	7	change_comment
27	Can delete comment	7	delete_comment
28	Can view comment	7	view_comment
29	Can add notification	8	add_notification
30	Can change notification	8	change_notification
31	Can delete notification	8	delete_notification
32	Can view notification	8	view_notification
33	Can add message	9	add_message
34	Can change message	9	change_message
35	Can delete message	9	delete_message
36	Can view message	9	view_message
37	Can add log entry	10	add_logentry
38	Can change log entry	10	change_logentry
39	Can delete log entry	10	delete_logentry
40	Can view log entry	10	view_logentry
41	Can add permission	11	add_permission
42	Can change permission	11	change_permission
43	Can delete permission	11	delete_permission
44	Can view permission	11	view_permission
45	Can add group	12	add_group
46	Can change group	12	change_group
47	Can delete group	12	delete_group
48	Can view group	12	view_group
49	Can add content type	13	add_contenttype
50	Can change content type	13	change_contenttype
51	Can delete content type	13	delete_contenttype
52	Can view content type	13	view_contenttype
53	Can add session	14	add_session
54	Can change session	14	change_session
55	Can delete session	14	delete_session
56	Can view session	14	view_session
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2021-05-24 17:51:08.796015-04	1	Soap	1	[{"added": {}}]	3	1
2	2021-05-24 17:51:28.813495-04	1	Moon	1	[{"added": {}}]	4	1
3	2021-05-24 18:25:12.19087-04	1	Soap	3		3	1
4	2021-05-24 18:28:34.508126-04	2	Soap	1	[{"added": {}}]	3	1
5	2021-05-24 18:28:50.914907-04	3	Lotion	1	[{"added": {}}]	3	1
6	2021-05-24 18:29:10.896867-04	4	Soap	1	[{"added": {}}]	3	1
7	2021-05-24 18:29:18.916687-04	2	Candle	2	[{"changed": {"fields": ["Title"]}}]	3	1
8	2021-05-24 18:32:37.938393-04	2	Pinecone	1	[{"added": {}}]	4	1
9	2021-05-24 18:33:42.275157-04	3	Blood Moon	1	[{"added": {}}]	4	1
10	2021-05-24 18:34:20.388404-04	4	Brown Sugar	1	[{"added": {}}]	4	1
11	2021-05-24 18:35:32.833961-04	5	Mountain Rain	1	[{"added": {}}]	4	1
12	2021-05-24 18:36:18.08264-04	6	Rose	1	[{"added": {}}]	4	1
13	2021-05-24 18:44:24.582736-04	7	Honey Grapefruit	1	[{"added": {}}]	4	1
14	2021-05-24 18:45:58.92673-04	8	Midnight Lavender	1	[{"added": {}}]	4	1
15	2021-05-24 18:46:52.565187-04	9	Morning Mist	1	[{"added": {}}]	4	1
16	2021-05-24 18:47:40.86929-04	10	Spring Blossom	1	[{"added": {}}]	4	1
17	2021-05-24 18:48:12.157644-04	11	Sweet Peach	1	[{"added": {}}]	4	1
18	2021-05-24 18:48:45.781489-04	12	Tropical Melon	1	[{"added": {}}]	4	1
19	2021-05-24 18:49:59.572884-04	13	Wood lotion	1	[{"added": {}}]	4	1
20	2021-05-24 18:50:35.240509-04	14	Citrus Lotion	1	[{"added": {}}]	4	1
21	2021-05-24 18:53:55.956089-04	15	Raptor Candle	1	[{"added": {}}]	4	1
22	2021-05-24 19:00:33.542701-04	16	Maple Syrup	1	[{"added": {}}]	4	1
23	2021-05-24 19:01:00.928774-04	17	Hand Cream	1	[{"added": {}}]	4	1
24	2021-05-24 19:02:23.63984-04	18	Hand Sanitizer	1	[{"added": {}}]	4	1
25	2021-05-24 19:03:15.687002-04	19	Stress Candle	1	[{"added": {}}]	4	1
26	2021-05-24 19:03:53.328623-04	20	Sandalwood Candle	1	[{"added": {}}]	4	1
27	2021-05-24 19:04:33.562512-04	21	Teakwood Candle	1	[{"added": {}}]	4	1
28	2021-05-24 19:28:30.89427-04	1	members	1	[{"added": {}}]	12	1
29	2021-05-24 19:29:04.593898-04	2	admin_user_gp	1	[{"added": {}}]	12	1
30	2021-05-24 19:29:51.115761-04	3	admin_Item_gp	1	[{"added": {}}]	12	1
31	2021-05-24 19:30:50.650116-04	4	admin_gp	1	[{"added": {}}]	12	1
32	2021-05-24 19:33:49.346154-04	2	nasr	1	[{"added": {}}]	1	1
33	2021-05-24 19:34:29.74356-04	3	fred	1	[{"added": {}}]	1	1
34	2021-05-24 19:35:01.913386-04	4	tristen	1	[{"added": {}}]	1	1
35	2021-05-24 19:37:10.333763-04	5	trevor	1	[{"added": {}}]	1	1
36	2021-05-24 19:37:50.286528-04	6	emilia	1	[{"added": {}}]	1	1
37	2021-05-24 19:38:19.202108-04	7	kristina	1	[{"added": {}}]	1	1
38	2021-05-24 19:38:50.581439-04	8	azadeh	1	[{"added": {}}]	1	1
39	2021-05-24 19:44:03.116684-04	9	kevin	1	[{"added": {}}]	1	1
40	2021-05-24 19:44:35.906192-04	10	bob	1	[{"added": {}}]	1	1
41	2021-05-24 20:55:47.029082-04	21	Teakwood Candle	2	[{"changed": {"fields": ["Owner"]}}]	4	1
42	2021-05-24 20:55:51.240753-04	20	Sandalwood Candle	2	[{"changed": {"fields": ["Owner"]}}]	4	1
43	2021-05-24 20:55:54.894794-04	19	Stress Candle	2	[{"changed": {"fields": ["Owner"]}}]	4	1
44	2021-05-24 20:55:58.472027-04	18	Hand Sanitizer	2	[{"changed": {"fields": ["Owner"]}}]	4	1
45	2021-05-24 20:56:02.419897-04	17	Hand Cream	2	[{"changed": {"fields": ["Owner"]}}]	4	1
46	2021-05-24 20:56:06.86338-04	16	Maple Syrup	2	[{"changed": {"fields": ["Owner"]}}]	4	1
47	2021-05-24 20:56:11.38034-04	15	Raptor Candle	2	[{"changed": {"fields": ["Owner"]}}]	4	1
48	2021-05-24 20:56:16.215312-04	13	Wood lotion	2	[{"changed": {"fields": ["Owner"]}}]	4	1
49	2021-05-24 20:56:21.139138-04	12	Tropical Melon	2	[{"changed": {"fields": ["Owner"]}}]	4	1
50	2021-05-24 20:56:24.982833-04	11	Sweet Peach	2	[{"changed": {"fields": ["Owner"]}}]	4	1
51	2021-05-24 20:56:32.224491-04	8	Midnight Lavender	2	[{"changed": {"fields": ["Owner"]}}]	4	1
52	2021-05-24 20:56:37.710083-04	9	Morning Mist	2	[{"changed": {"fields": ["Owner"]}}]	4	1
53	2021-05-24 20:56:41.556868-04	5	Mountain Rain	2	[{"changed": {"fields": ["Owner"]}}]	4	1
54	2021-05-24 20:56:45.249129-04	4	Brown Sugar	2	[{"changed": {"fields": ["Owner"]}}]	4	1
55	2021-05-24 20:56:59.536547-04	14	Citrus Lotion	2	[{"changed": {"fields": ["Owner"]}}]	4	1
56	2021-05-24 20:57:04.494982-04	6	Rose	2	[{"changed": {"fields": ["Owner"]}}]	4	1
57	2021-05-24 20:57:09.534354-04	3	Blood Moon	2	[{"changed": {"fields": ["Owner"]}}]	4	1
58	2021-05-24 20:57:14.290807-04	3	Blood Moon	2	[]	4	1
59	2021-05-24 21:00:23.37638-04	16	Maple Syrup	2	[{"changed": {"fields": ["Owner"]}}]	4	1
60	2021-05-24 21:00:45.485978-04	5	Mountain Rain	2	[{"changed": {"fields": ["Owner"]}}]	4	1
61	2021-05-24 21:00:49.403574-04	2	Pinecone	2	[{"changed": {"fields": ["Owner"]}}]	4	1
62	2021-05-24 21:01:29.879277-04	2	nasr	3		1	1
63	2021-05-24 21:02:35.27924-04	16	nasr	2	[{"changed": {"fields": ["Groups", "First name", "Last name", "Email address"]}}]	1	1
64	2021-05-24 21:04:35.006037-04	10	bob	2	[{"changed": {"fields": ["Password"]}}]	1	16
65	2021-05-24 21:04:43.823976-04	9	kevin	2	[{"changed": {"fields": ["Password"]}}]	1	16
66	2021-05-24 21:04:47.482195-04	8	azadeh	2	[{"changed": {"fields": ["Password"]}}]	1	16
67	2021-05-24 21:04:53.941226-04	7	kristina	2	[{"changed": {"fields": ["Password"]}}]	1	16
68	2021-05-24 21:04:57.353722-04	6	emilia	2	[{"changed": {"fields": ["Password"]}}]	1	16
69	2021-05-24 21:05:01.45196-04	5	trevor	2	[{"changed": {"fields": ["Password"]}}]	1	16
70	2021-05-24 21:05:05.072335-04	4	tristen	2	[{"changed": {"fields": ["Password"]}}]	1	16
71	2021-05-24 21:05:08.404282-04	3	fred	2	[{"changed": {"fields": ["Password"]}}]	1	16
72	2021-05-24 21:07:40.479002-04	17	trevor2	2	[{"changed": {"fields": ["Password"]}}]	1	1
73	2021-05-24 21:08:03.02799-04	10	bob	2	[{"changed": {"fields": ["Password"]}}]	1	1
74	2021-05-24 21:08:09.044436-04	9	kevin	2	[{"changed": {"fields": ["Password"]}}]	1	1
75	2021-05-24 21:08:12.323309-04	8	azadeh	2	[{"changed": {"fields": ["Password"]}}]	1	1
76	2021-05-24 21:08:20.860138-04	7	kristina	2	[{"changed": {"fields": ["Password"]}}]	1	1
77	2021-05-24 21:08:24.309702-04	6	emilia	2	[{"changed": {"fields": ["Password"]}}]	1	1
78	2021-05-24 21:08:27.663047-04	5	trevor	2	[{"changed": {"fields": ["Password"]}}]	1	1
79	2021-05-24 21:08:30.676584-04	4	tristen	2	[{"changed": {"fields": ["Password"]}}]	1	1
80	2021-05-24 21:08:34.787071-04	3	fred	2	[{"changed": {"fields": ["Password"]}}]	1	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	Administration	userprofile
2	users	profile
3	items	category
4	items	item
5	items	rate
6	items	images
7	items	comment
8	messaging	notification
9	messaging	message
10	admin	logentry
11	auth	permission
12	auth	group
13	contenttypes	contenttype
14	sessions	session
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-05-24 17:49:25.707857-04
2	contenttypes	0002_remove_content_type_name	2021-05-24 17:49:25.716851-04
3	auth	0001_initial	2021-05-24 17:49:25.737851-04
4	auth	0002_alter_permission_name_max_length	2021-05-24 17:49:25.761851-04
5	auth	0003_alter_user_email_max_length	2021-05-24 17:49:25.76785-04
6	auth	0004_alter_user_username_opts	2021-05-24 17:49:25.77285-04
7	auth	0005_alter_user_last_login_null	2021-05-24 17:49:25.777851-04
8	auth	0006_require_contenttypes_0002	2021-05-24 17:49:25.779852-04
9	auth	0007_alter_validators_add_error_messages	2021-05-24 17:49:25.786851-04
10	auth	0008_alter_user_username_max_length	2021-05-24 17:49:25.791851-04
11	auth	0009_alter_user_last_name_max_length	2021-05-24 17:49:25.796851-04
12	auth	0010_alter_group_name_max_length	2021-05-24 17:49:25.808851-04
13	auth	0011_update_proxy_permissions	2021-05-24 17:49:25.81385-04
14	auth	0012_alter_user_first_name_max_length	2021-05-24 17:49:25.818851-04
15	Administration	0001_initial	2021-05-24 17:49:25.83685-04
16	admin	0001_initial	2021-05-24 17:49:25.87085-04
17	admin	0002_logentry_remove_auto_add	2021-05-24 17:49:25.88285-04
18	admin	0003_logentry_add_action_flag_choices	2021-05-24 17:49:25.88885-04
19	items	0001_initial	2021-05-24 17:49:25.935851-04
20	messaging	0001_initial	2021-05-24 17:49:25.981852-04
21	sessions	0001_initial	2021-05-24 17:49:25.998851-04
22	users	0001_initial	2021-05-24 17:49:26.018851-04
23	users	0002_auto_20210524_2016	2021-05-24 20:16:36.949891-04
24	users	0003_auto_20210524_2018	2021-05-24 20:18:30.664475-04
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
cb09mj2bamvxhb8apw8r77hkqqt028kn	.eJxVjDEOAiEQRe9CbQgDjIClvWcgw4CyaiBZdivj3XWTLbT9773_EpHWpcZ1lDlOWZwEisPvlogfpW0g36nduuTelnlKclPkToe89Fye5939O6g06rdm1kYDJvBJWeeC8T5AYM4IrIGMulqluKCBEix460AxUvDu6FClguL9AbbKNn0:1llMhF:iOAbEYa_bavWDNHvxQTitY4K5H3UYHfxW4emBB-_WWA	2021-06-07 22:21:29.65892-04
\.


--
-- Data for Name: items_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_category (id, title, keywords, description) FROM stdin;
3	Lotion	Hydrating, Renewing	Formulated for any skin type!
4	Soap	Aromatherapy, Relaxing, Mood	3-Wick candles guaranteed to put you at ease!
2	Candle	Homemade, Organic, Bar	Gorgeous Homemade Soaps from Montreal Soap Co.
\.


--
-- Data for Name: items_comment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_comment (id, subject, comment, created_on, item_id, user_id) FROM stdin;
1	Best Color	So red!	2021-05-24 21:37:41.25821-04	3	5
2			2021-05-24 21:48:38.241592-04	4	5
3	Best lotion ever!	So smooth	2021-05-24 21:49:33.019711-04	14	5
4	Overpriced	Everything else is under $20	2021-05-24 21:56:00.236519-04	18	5
\.


--
-- Data for Name: items_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_images (id, title, image, item_id) FROM stdin;
\.


--
-- Data for Name: items_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_item (id, title, description, status, price, address, image, created_at, updated_at, category_id, owner_id) FROM stdin;
7	Honey Grapefruit	Made with the finest honey!	Available	2.99	123 Boul Langelier	items/honeygrapefruit1.jpg	2021-05-24 18:44:24.580734-04	2021-05-24 18:44:24.580734-04	4	1
10	Spring Blossom	Ready to spring into action?	Available	7.00	Iceland	items/springblossom1.jpg	2021-05-24 18:47:40.868291-04	2021-05-24 18:47:40.868291-04	4	1
21	Teakwood Candle	Enhance every room with this smell!	Available	5.99	New York, USA	items/teakwoodcandle.jpg	2021-05-24 19:04:33.56051-04	2021-05-24 20:55:47.028084-04	2	5
20	Sandalwood Candle	A unique scent	Available	5.99	New York, USA	items/sandalwoodcandle.jpg	2021-05-24 19:03:53.326619-04	2021-05-24 20:55:51.239752-04	2	6
19	Stress Candle	Feel at bliss with your surroundings	Available	5.99	888 Elmo Street	items/stresscandle.jpg	2021-05-24 19:03:15.685004-04	2021-05-24 20:55:54.893793-04	2	5
18	Hand Sanitizer	For all your hand cleaning	Available	25.00	New York, USA	items/lotionno6.jpg	2021-05-24 19:02:23.637841-04	2021-05-24 20:55:58.471029-04	3	8
17	Hand Cream	Perfect for delicate hands	Available	12.00	New York, USA	items/lotionclearbottle.jpg	2021-05-24 19:01:00.927752-04	2021-05-24 20:56:02.418844-04	3	10
15	Raptor Candle	Dinosaur scented candle	Available	12.00	45 Eclipse Road	items/raptorcandle.jpg	2021-05-24 18:53:55.954086-04	2021-05-24 20:56:11.37934-04	2	4
13	Wood lotion	Get all the finest oakness from your lotion	Available	7.95	New York, USA	items/woodlotion.jpg	2021-05-24 18:49:59.570882-04	2021-05-24 20:56:16.214312-04	3	7
12	Tropical Melon	Go on vacation while in the shower	Available	20.00	California, USA	items/tropicalmelon1.jpg	2021-05-24 18:48:45.78049-04	2021-05-24 20:56:21.138097-04	4	7
11	Sweet Peach	Peachy, as always	Available	8.00	China	items/sweetpeach1.jpg	2021-05-24 18:48:12.156643-04	2021-05-24 20:56:24.981852-04	4	5
8	Midnight Lavender	Feel what dreams feel like	Available	5.00	Quebec City	items/midnightlavender1.jpg	2021-05-24 18:45:58.924742-04	2021-05-24 20:56:32.22349-04	4	6
9	Morning Mist	Wake up to this beautiful smell and feel prepared for the day!	Available	6.00	New York, USA	items/morningmist1.jpg	2021-05-24 18:46:52.564187-04	2021-05-24 20:56:37.709093-04	4	9
4	Brown Sugar	Sweet Pink	Available	4.99	3 Candy Street	items/brownsugar1_nOUneHP.jpg	2021-05-24 18:34:20.387404-04	2021-05-24 20:56:45.248129-04	4	6
14	Citrus Lotion	Described as somewhat Orangy	Available	3.95	New York, USA	items/citruslotion.jpg	2021-05-24 18:50:35.239513-04	2021-05-24 20:56:59.536003-04	3	3
6	Rose	Valentines Day Come Faster !!	Available	11.99	333 Cupid Rd.	items/rose1_Wvw7445.jpg	2021-05-24 18:36:18.081689-04	2021-05-24 20:57:04.49398-04	4	10
3	Blood Moon	Bloody Red	Available	6.99	45 Eclipse Road	items/bloodmoon1_eMdLLqh.jpg	2021-05-24 18:33:42.273156-04	2021-05-24 20:57:14.289798-04	4	6
16	Maple Syrup	Not really, or is it?	Available	0.00	45 Forest Drive	items/lotionamberbottle.jpg	2021-05-24 19:00:33.540671-04	2021-05-24 21:00:23.375379-04	3	4
5	Mountain Rain	Misty Morning Rain	Available	5.99	888 Elmo Street	items/mountainrain1_Dgjwkym.jpg	2021-05-24 18:35:32.832962-04	2021-05-24 21:00:45.48498-04	4	8
2	Pinecone	Beige, Green, Brown Bar Soap with a lovely Pine tree scent.    A must-have!	Available	12.99	45 Forest Drive	items/pinecone1_vcC2txn.jpg	2021-05-24 18:32:37.936428-04	2021-05-24 21:00:49.401588-04	4	10
\.


--
-- Data for Name: items_rate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_rate (id, rate, item_id, user_id) FROM stdin;
1	5	3	5
2	4	6	5
3	1	3	5
4	5	4	5
5	5	14	5
6	4	18	5
\.


--
-- Data for Name: messaging_message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.messaging_message (id, title, text, date, receiver_id, sender_id) FROM stdin;
1	Hello	Hope you like our project :D	2021-05-24 22:06:20.418766-04	16	5
\.


--
-- Data for Name: messaging_notification; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.messaging_notification (id, message, user_id) FROM stdin;
\.


--
-- Data for Name: users_profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_profile (id, image, user_id) FROM stdin;
1	default.png	1
8	profile_pics/default.png	16
10	profile_pics/1725_impasta.png	5
\.


--
-- Name: Administration_userprofile_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Administration_userprofile_groups_id_seq"', 19, true);


--
-- Name: Administration_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Administration_userprofile_id_seq"', 18, true);


--
-- Name: Administration_userprofile_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Administration_userprofile_user_permissions_id_seq"', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 4, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 93, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 56, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 80, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 14, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 24, true);


--
-- Name: items_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_category_id_seq', 4, true);


--
-- Name: items_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_comment_id_seq', 4, true);


--
-- Name: items_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_images_id_seq', 1, false);


--
-- Name: items_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_item_id_seq', 21, true);


--
-- Name: items_rate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_rate_id_seq', 6, true);


--
-- Name: messaging_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.messaging_message_id_seq', 1, true);


--
-- Name: messaging_notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.messaging_notification_id_seq', 1, false);


--
-- Name: users_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_profile_id_seq', 10, true);


--
-- Name: Administration_userprofile_groups Administration_userprofi_userprofile_id_group_id_8b16a33f_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_groups"
    ADD CONSTRAINT "Administration_userprofi_userprofile_id_group_id_8b16a33f_uniq" UNIQUE (userprofile_id, group_id);


--
-- Name: Administration_userprofile_user_permissions Administration_userprofi_userprofile_id_permissio_968ce4a6_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_user_permissions"
    ADD CONSTRAINT "Administration_userprofi_userprofile_id_permissio_968ce4a6_uniq" UNIQUE (userprofile_id, permission_id);


--
-- Name: Administration_userprofile_groups Administration_userprofile_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_groups"
    ADD CONSTRAINT "Administration_userprofile_groups_pkey" PRIMARY KEY (id);


--
-- Name: Administration_userprofile Administration_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile"
    ADD CONSTRAINT "Administration_userprofile_pkey" PRIMARY KEY (id);


--
-- Name: Administration_userprofile_user_permissions Administration_userprofile_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_user_permissions"
    ADD CONSTRAINT "Administration_userprofile_user_permissions_pkey" PRIMARY KEY (id);


--
-- Name: Administration_userprofile Administration_userprofile_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile"
    ADD CONSTRAINT "Administration_userprofile_username_key" UNIQUE (username);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: items_category items_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_category
    ADD CONSTRAINT items_category_pkey PRIMARY KEY (id);


--
-- Name: items_comment items_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_comment
    ADD CONSTRAINT items_comment_pkey PRIMARY KEY (id);


--
-- Name: items_images items_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_images
    ADD CONSTRAINT items_images_pkey PRIMARY KEY (id);


--
-- Name: items_item items_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_item
    ADD CONSTRAINT items_item_pkey PRIMARY KEY (id);


--
-- Name: items_rate items_rate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_rate
    ADD CONSTRAINT items_rate_pkey PRIMARY KEY (id);


--
-- Name: messaging_message messaging_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messaging_message
    ADD CONSTRAINT messaging_message_pkey PRIMARY KEY (id);


--
-- Name: messaging_notification messaging_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messaging_notification
    ADD CONSTRAINT messaging_notification_pkey PRIMARY KEY (id);


--
-- Name: users_profile users_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_profile
    ADD CONSTRAINT users_profile_pkey PRIMARY KEY (id);


--
-- Name: users_profile users_profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_profile
    ADD CONSTRAINT users_profile_user_id_key UNIQUE (user_id);


--
-- Name: Administration_userprofile_groups_group_id_8183c112; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Administration_userprofile_groups_group_id_8183c112" ON public."Administration_userprofile_groups" USING btree (group_id);


--
-- Name: Administration_userprofile_groups_userprofile_id_6b523538; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Administration_userprofile_groups_userprofile_id_6b523538" ON public."Administration_userprofile_groups" USING btree (userprofile_id);


--
-- Name: Administration_userprofile_permission_id_3c155a59; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Administration_userprofile_permission_id_3c155a59" ON public."Administration_userprofile_user_permissions" USING btree (permission_id);


--
-- Name: Administration_userprofile_username_caa8f38c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Administration_userprofile_username_caa8f38c_like" ON public."Administration_userprofile" USING btree (username varchar_pattern_ops);


--
-- Name: Administration_userprofile_userprofile_id_d4eab128; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Administration_userprofile_userprofile_id_d4eab128" ON public."Administration_userprofile_user_permissions" USING btree (userprofile_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: items_comment_item_id_9f8f7e16; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX items_comment_item_id_9f8f7e16 ON public.items_comment USING btree (item_id);


--
-- Name: items_comment_user_id_d99db8b7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX items_comment_user_id_d99db8b7 ON public.items_comment USING btree (user_id);


--
-- Name: items_images_item_id_809c1b37; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX items_images_item_id_809c1b37 ON public.items_images USING btree (item_id);


--
-- Name: items_item_category_id_98ac533d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX items_item_category_id_98ac533d ON public.items_item USING btree (category_id);


--
-- Name: items_item_owner_id_c5de0655; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX items_item_owner_id_c5de0655 ON public.items_item USING btree (owner_id);


--
-- Name: items_rate_item_id_e1f259f6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX items_rate_item_id_e1f259f6 ON public.items_rate USING btree (item_id);


--
-- Name: items_rate_user_id_f3c658f7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX items_rate_user_id_f3c658f7 ON public.items_rate USING btree (user_id);


--
-- Name: messaging_message_receiver_id_00da1113; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX messaging_message_receiver_id_00da1113 ON public.messaging_message USING btree (receiver_id);


--
-- Name: messaging_message_sender_id_7a7088e6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX messaging_message_sender_id_7a7088e6 ON public.messaging_message USING btree (sender_id);


--
-- Name: messaging_notification_user_id_7f01f50a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX messaging_notification_user_id_7f01f50a ON public.messaging_notification USING btree (user_id);


--
-- Name: Administration_userprofile_groups Administration_userp_group_id_8183c112_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_groups"
    ADD CONSTRAINT "Administration_userp_group_id_8183c112_fk_auth_grou" FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Administration_userprofile_user_permissions Administration_userp_permission_id_3c155a59_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_user_permissions"
    ADD CONSTRAINT "Administration_userp_permission_id_3c155a59_fk_auth_perm" FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Administration_userprofile_groups Administration_userp_userprofile_id_6b523538_fk_Administr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_groups"
    ADD CONSTRAINT "Administration_userp_userprofile_id_6b523538_fk_Administr" FOREIGN KEY (userprofile_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Administration_userprofile_user_permissions Administration_userp_userprofile_id_d4eab128_fk_Administr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Administration_userprofile_user_permissions"
    ADD CONSTRAINT "Administration_userp_userprofile_id_d4eab128_fk_Administr" FOREIGN KEY (userprofile_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_Administr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT "django_admin_log_user_id_c564eba6_fk_Administr" FOREIGN KEY (user_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: items_comment items_comment_item_id_9f8f7e16_fk_items_item_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_comment
    ADD CONSTRAINT items_comment_item_id_9f8f7e16_fk_items_item_id FOREIGN KEY (item_id) REFERENCES public.items_item(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: items_comment items_comment_user_id_d99db8b7_fk_Administration_userprofile_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_comment
    ADD CONSTRAINT "items_comment_user_id_d99db8b7_fk_Administration_userprofile_id" FOREIGN KEY (user_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: items_images items_images_item_id_809c1b37_fk_items_item_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_images
    ADD CONSTRAINT items_images_item_id_809c1b37_fk_items_item_id FOREIGN KEY (item_id) REFERENCES public.items_item(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: items_item items_item_category_id_98ac533d_fk_items_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_item
    ADD CONSTRAINT items_item_category_id_98ac533d_fk_items_category_id FOREIGN KEY (category_id) REFERENCES public.items_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: items_item items_item_owner_id_c5de0655_fk_Administration_userprofile_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_item
    ADD CONSTRAINT "items_item_owner_id_c5de0655_fk_Administration_userprofile_id" FOREIGN KEY (owner_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: items_rate items_rate_item_id_e1f259f6_fk_items_item_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_rate
    ADD CONSTRAINT items_rate_item_id_e1f259f6_fk_items_item_id FOREIGN KEY (item_id) REFERENCES public.items_item(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: items_rate items_rate_user_id_f3c658f7_fk_Administration_userprofile_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_rate
    ADD CONSTRAINT "items_rate_user_id_f3c658f7_fk_Administration_userprofile_id" FOREIGN KEY (user_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: messaging_message messaging_message_receiver_id_00da1113_fk_Administr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messaging_message
    ADD CONSTRAINT "messaging_message_receiver_id_00da1113_fk_Administr" FOREIGN KEY (receiver_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: messaging_message messaging_message_sender_id_7a7088e6_fk_Administr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messaging_message
    ADD CONSTRAINT "messaging_message_sender_id_7a7088e6_fk_Administr" FOREIGN KEY (sender_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: messaging_notification messaging_notificati_user_id_7f01f50a_fk_Administr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messaging_notification
    ADD CONSTRAINT "messaging_notificati_user_id_7f01f50a_fk_Administr" FOREIGN KEY (user_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_profile users_profile_user_id_2112e78d_fk_Administration_userprofile_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_profile
    ADD CONSTRAINT "users_profile_user_id_2112e78d_fk_Administration_userprofile_id" FOREIGN KEY (user_id) REFERENCES public."Administration_userprofile"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

