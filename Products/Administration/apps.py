# Trevor McCubbin - 1542960
# For 420-420 Python

from django.apps import AppConfig

class AdministrationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Administration'
