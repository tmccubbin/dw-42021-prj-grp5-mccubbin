# Trevor McCubbin - 1542960
# For 420-420 Python

from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

# This overrides the default auth user to add a flagged property
class UserProfile(AbstractUser):
    flagged = models.BooleanField(default=False)
