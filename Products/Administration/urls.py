# Trevor McCubbin - 1542960
# For 420-420 Python

from django.urls import path
from . import views

urlpatterns = [
    #need to specify first part of path to be the url you want, '' is index
    path('', views.AdministrationView.as_view(), name='administration'),
    path('delete/<int:pk>', views.deleteUser, name='delete-user'),
    path('flag/<int:pk>', views.flagUser, name='flag-user'),
    path('create-user', views.RegisterNewUserAdmin.as_view(), name='create-user'),
    path('edit/<int:pk>', views.EditUserView.as_view(), name='edit-user'),
    path('modify-group/<int:pk>', views.ModifyGroupView.as_view(), name='modify-group'),
    path('modify-group/add/<int:pk>/<str:group_name>', views.AddGroup, name='add-group'),
    path('modify-group/remove/<int:pk>/<str:group_name>', views.RemoveGroup, name='remove-group'),
    path('error', views.ErrorPageView.as_view(), name='error'),
]