# Trevor McCubbin - 1542960
# For 420-420 Python

from django import forms
from django.contrib.auth import get_user_model
User = get_user_model()

# This is called when the edit details button is pressed in users list
# allows the admin to change that users email, and names
class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

# This is called when the register new button is clicked in the users list view.
# Creates a new User in the db on save
class UserRegisterForm(forms.ModelForm):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'password', 'first_name', 'last_name', 'email']
