# Trevor McCubbin - 1542960
# For 420-420 Python

from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from .forms import UserUpdateForm, UserRegisterForm
import logging
from datetime import datetime
from django.contrib.auth import get_user_model
from django.shortcuts import render, get_object_or_404, redirect

from users.models import Profile

# Get the User model currently being used by the app (needed because an abstract user model is defined)
User = get_user_model()

# Get an instance of a logger
logger = logging.getLogger('admin')

# Create your views here.

# This is the default view of Users List
class AdministrationView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):

    # checks to see if the request user has this permission
    permission_required = 'Administration.view_userprofile'

    template_name = "Administration/administration.html"
    raise_exception = False  # Redirect to login

    # if the requesting user does not have the permission to view this page it redirects them to an error page
    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            return redirect('/administration/error')
        else:
            return redirect('/login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Administration'
        context['page_name'] = 'Users List'
        # this checks to see if the requesting user is part of the admin_gp group or if they are a superuser
        if self.request.user.groups.filter(name="admin_gp") or self.request.user.is_superuser:
            # returns all users no matter which group
            context['users_list'] = User.objects.all().order_by('pk')
        else:
            # returns all users in the members group
            context['users_list'] = User.objects.filter(groups__name='members').order_by('pk')
        return context

@login_required
def deleteUser(request, pk):
    # this checks if the person trying to access this method is only a member (sinec they can fake it by inputting the proper url)
    if request.user.groups.filter(name="member") and request.user.groups.all().count == 1:
        return redirect('/administration/error')
    else:
        user = User.objects.get(id=pk)
        profile = Profile.objects.get_or_create(user=user)
        user.delete()
        logger.info(str(datetime.now()) + ': <' + str(user.username) + '> was deleted by: ' + str(request.user.username))
        return redirect('/administration')

@login_required
def flagUser(request, pk):
    # this checks if the person trying to access this method is only a member (sinec they can fake it by inputting the proper url)
    if request.user.groups.filter(name="member") and request.user.groups.all().count == 1:
        return redirect('/administration/error')
    else:
        user = User.objects.get(id=pk)
        # inverse the boolean
        user.flagged = not user.flagged
        user.save()
        logger.info(str(datetime.now()) + ': <' + str(user.username) + '> was flagged by: ' + str(request.user.username))
        return redirect('/administration')

class EditUserView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):

    # checks to see if the request user has this permission
    permission_required = 'Administration.change_userprofile'

    template_name = "Administration/editUser.html"
    raise_exception = False  # Redirect to login

    # if the requesting user does not have the permission to view this page it redirects them to an error page
    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            return redirect('/administration/error')
        else:
            return redirect('/login')

    def get_context_data(self, pk, **kwargs):
        user = User.objects.get(id=pk)
        form = UserUpdateForm(instance=user)
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Edit User'
        context['page_name'] = 'Edit User'
        context['user'] = user
        context['form'] = form
        return context

    def post(self, request, pk):
        user = User.objects.get(id=pk)
        form = UserUpdateForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            logger.info(str(datetime.now()) + ': <' + str(user.username) + '> was edited by: ' + str(request.user.username))
            return redirect('administration')


class ModifyGroupView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):

    # checks to see if the request user has this permission
    permission_required = 'Administration.change_userprofile'

    template_name = "Administration/modifyGroup.html"
    raise_exception = False  # Redirect to login

    # if the requesting user does not have the permission to view this page it redirects them to an error page
    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            return redirect('/administration/error')
        else:
            return redirect('/login')

    def get_context_data(self, pk, **kwargs):
        user = User.objects.get(id=pk)
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Modify User Group'
        context['page_name'] = 'Modify User Group'
        context['user'] = user
        if self.request.user.groups.filter(name="admin_gp") or self.request.user.is_superuser:
            context['groups'] = Group.objects.all().order_by('pk')
        else:
            context['groups'] = Group.objects.filter(name='members')
        return context

@login_required
def AddGroup(request, pk, group_name):
    # this checks if the person trying to access this method is only a member (sinec they can fake it by inputting the proper url)
    if request.user.groups.filter(name="member") and request.user.groups.all().count == 1:
        return redirect('/administration/error')
    else:
        user = User.objects.get(id=pk)
        group = Group.objects.get(name=group_name)
        user.groups.add(group)
        logger.info(str(datetime.now()) + ': Group: <' + str(group_name) + '> was added to user: <' + str(user.username) + '> by: ' + str(request.user.username))
        return redirect('/administration/modify-group/' + str(pk))

@login_required
def RemoveGroup(request, pk, group_name):
    # this checks if the person trying to access this method is only a member (sinec they can fake it by inputting the proper url)
    if request.user.groups.filter(name="member") and request.user.groups.all().count == 1:
        return redirect('/administration/error')
    else:
        user = User.objects.get(id=pk)
        group = Group.objects.get(name=group_name)
        user.groups.remove(group)
        logger.info(str(datetime.now()) + ': Group: <' + str(group_name) + '> was removed from user: <' + str(user.username) + '> by: ' + str(request.user.username))
        return redirect('/administration/modify-group/' + str(pk))

class RegisterNewUserAdmin(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):

    # checks to see if the request user has this permission
    permission_required = 'Administration.add_userprofile'

    template_name = "Administration/registerUser.html"
    raise_exception = False  # Redirect to login

    # if the requesting user does not have the permission to view this page it redirects them to an error page
    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            return redirect('/administration/error')
        else:
            return redirect('/login')

    def get_context_data(self, **kwargs):
        form = UserRegisterForm()
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Register User'
        context['page_name'] = 'Register User'
        context['form'] = form
        return context

    def post(self, request):
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            # this part adds the group members to any new user
            group = Group.objects.get(name='members')
            form.save()
            user = User.objects.get(username=username)
            user.groups.add(group)
            logger.info(str(datetime.now()) + ': User <' + str(username) + '> was created by: ' + str(request.user.username))
            return redirect('administration')

class ErrorPageView(TemplateView):

    template_name = "Administration/error.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Error!'
        context['page_name'] = 'Error Page'
        context['error'] = "You do not have permission to view this page!"
        return context
